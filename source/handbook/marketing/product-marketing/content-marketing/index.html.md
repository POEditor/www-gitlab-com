---
layout: markdown_page
title: "Content Marketing"
---

Welcome to the Content Marketing Handbook

[Up one level to the Product Marketing Handbook](../) {::comment} TIP FOR ONE LEVEL UP :)  {:/comment}

## On this page
{:.no_toc}

* Will be replaced with the ToC, excluding the "On this page" header
{:toc}

## Introduction<a name="intro"></a>

The philosophy for Content Marketing at GitLab is to help bring insider
knowledge and implicit practice to the hands of developers getting acquainted
with GitLab, and decision makers considering GitLab.

We want to meet each user where they are now, and help them be the most
efficient they can be with our tools.
We want to help their teams realize their creative and collaborative goals.

We also want to address the whole organization, the coal-face of development
or in decision making roles. GitLab is not only used for code development and
review. Those same tools are used by team members to track progress, create
documentation and collaborate on projects. When we show how we work "inside
GitLab" we also model how to use our software.

## 2016 activities<a name="2016"></a>

- Publish an active [blog](blog/) with useful content relevant to GitLab users.
- Host [webcasts](webcasts/)
which welcome newcomers, celebrate with existing users, and provide access to expertise.
- Publish a twice-monthly email newsletter you can sign up to on [our contact page](https://about.gitlab.com/contact/).

## What is the webcast program at GitLab?<a name="webcast"></a>

-   The webcast program consists of regular online events for GitLab users and community members.
-   The webcasts are recorded and distributed to GitLab users, and can be referred to in the Resource Library as part of GitLab University

- [How to Schedule a webcast](#schedule)
- [Technical requirements to view a webcast](#view)

### Uploading video files to the GitLab YouTube channel

- You must be a manager of the [GitLab YouTube channel](https://www.youtube.com/channel/UCnMGQ8QHMAnVIsI3xJrihhg) to upload.
- To be added as a manager of the GitLab YouTube channel, please request the channel owner add you as manager.
- The CMO is the GitLab YouTube channel owner.
- [How to add or remove users to a channel](https://support.google.com/youtube/answer/4628007?hl=en)
- The direct link to add and remove users for the manager is [here](https://plus.google.com/u/0/b/107491817347842790515/pages/settings/admin).
- If you’d like to add video bumpers to the beginning or end of your video, please download from this [issue](https://gitlab.com/gitlab-com/gitlab-artwork/tree/master/video-bumpers).

### Monthly Program

This program is in development. For the first month, January, we'll start with one webcast, then add another and build up to the full schedule.

### Welcome to GitLab - 20 mins (coming soon!)

-   Live demo run.
-   Similar base demo script each month.
    -   How to use GitLab - essential demo.
    -   This month’s Q+A (commonly asked questions, recent questions from twitter)
    -   GitLab’s products and services.
    -   How to find stuff in the GitLab; getting help, direction, participation.
    -   Community welcome mat: How to meet other GitLab users what events we’ll be at, how about you?
-   Aimed at developers or decision makers who have signed up in the last 30 days, inviting them into the community.

### Release Party - 30 - 40 mins

-   Monthly Thursday following a release.
-   Present highlights from the new features.
-   Refer to any resources, docs, screencasts, etc.
-   Guest speakers from the dev team about the new features.
-   Highlight contributors and the MVP for that month.
-   New contributors welcomed.
-   Q+A from audience.

### GitLab Tutorials - 40 mins - 1 hour

-   Live presentation, demo or discussion on monthly in-depth learning theme.
-   Preceded by 3 weeks of blog posts, screencasts, tutorial and an invitation which leads to the online event.
-   Live event includes: Guest speaker / interview / presentation / demo as appropriate to topic.
-   Q+A from audience. Survey to GitLab users on the topic if appropriate.
-   After event: Blog post of findings from the Q+A, results of survey.
-   Roll-up content into an downloadable ebook, course or other way to make the content more easily accessed and reviewed.

## Scheduling webcasts<a name="schedule"></a>

- Webcasts are on Thursdays, 17:00 UTC (9am PST, 6pm CET)
- Panelists should arrive 15 mins before the webcast for a final sound check
- Panelists should participate in a rehearsal before the webcast

#### Webcast configuration checklist

- Set up webinar in Gotowebinar by cloning the most recent webcast.
- Add a welcome message to attendees
- Upload any handouts (up to 5)
- Upload images, branding and speaker photos (must be jpg or gif and the exact dimensions)

#### Create a calendar event

GoToWebinar doesn't send out a Google calendar friendly invite

- Create an invitation for speakers in Google calendar
- Include their actual URL from their invite.
- Do the same for the rehearsal invitation.


#### Create the program in Marketo

- Clone the last campaign
- Connect the webcast campaign to Gotowebinar. Webcast > Event Settings > Event partner
- Correct version number and invitation date and details for assets:
    -   The landing page
    -   The confirmation email
- Update the hero form on the landing page to the correct form.
- Final check: If the lightbulb is not "on" (yellow), then it's not doing anything.
Check the smart list and flow first. To activate: Click the "registered" smart campaign -> "Schedule" tab -> "Activate" button

#### Promote

- Publish to Facebook
- Schedule tweets
- Create a blog post
- Add to next newsletter

#### As the event starts

- Promote [the appropriate attendees to panelist](https://support.citrixonline.com/en_US/webinar/knowledge_articles/000027765)
- Conduct a sound check and sharing check for anyone who will present.
- Organizers and Panelists are listed in the "Staff" tab and they can mute and unmute attendees, and see questions sent to the Panelists, etc.

## After the webcast<a name="followup"></a>

- Process the recording as .mov
- Upload to YouTube and slides to Speakerdeck
- Blog post to share the recording and slides


### Viewing webcasts<a name="view"></a>

- [Citrix Online system requirements](https://support.citrixonline.com/webinar/all_files/G2W010003)
- Using GoToWebinar Instant Join, Linux/Unbuntu users can view in a web browser.
- Linux users should use Chromium to view the browser.
